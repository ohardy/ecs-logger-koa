module.exports = {
  testMatch: ['**/__tests__/**/*.test.ts'],
  coveragePathIgnorePatterns: ['__tests__/data.ts'],
  coverageReporters: ['json', 'text', 'html'],
  moduleFileExtensions: ['js', 'json', 'ts'],
  testPathIgnorePatterns: ['/node_modules/', '<rootDir>/lib/'],
  transform: {
    '^.+\\.[t|j]sx?$': 'ts-jest',
  },
};
