import http from 'http';

import Koa from 'koa';
import { createLogger } from 'ecs-logger';
import { VError } from 'verror';

import { useKoaLogger } from '../src';

const app = new Koa();
app.proxy = true;

const logger = useKoaLogger(app, {
  logger: createLogger({
    ecsDocument: {
      trace: {
        id: 'errer',
      },
    },
  }),
});

/* eslint-disable consistent-return */
app.use((ctx): void | Promise<void> => {
  if (ctx.url.startsWith('/status/')) {
    const value = ctx.url.substr(8);
    ctx.status = parseInt(value, 10);
  } else if (ctx.url === '/error') {
    throw new Error('An error !!!');
  } else if (ctx.url === '/verror') {
    const err1 = new VError('something bad happened');
    const err2 = new VError(
      {
        name: 'ConnectionError',
        cause: err1,
        info: {
          errno: 'ECONNREFUSED',
          remote_ip: '127.0.0.1',
          port: 215,
        },
      },
      'failed to connect to "%s:%d"',
      '127.0.0.1',
      215
    );
    throw new VError(err2, 'An error !!!');
  } else if (ctx.url === '/wait') {
    return new Promise((resolve) => {
      setTimeout(resolve, 1000);
    });
  } else {
    ctx.status = 404;
  }
  ctx.logger.info('An info here');
  ctx.body = ctx.url;
});
/* eslint-enable consistent-return */

const server = http.createServer(app.callback());

const address = '127.0.0.1';
const port = 3434;

server.listen(port, address, () => {
  logger.info(`Running a web server at http://${address}:${port}`);
});
