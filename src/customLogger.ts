import { createLogger, Logger } from 'ecs-logger';

export function getCustomLogger(logger?: Logger) {
  let newLogger = logger || createLogger();

  newLogger = newLogger.createSubLogger({
    ecsDocument: {
      event: {
        module: 'koa',
      },
    },
  });

  return newLogger;
}
