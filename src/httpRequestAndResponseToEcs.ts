import url from 'url';
import { IncomingMessage, ServerResponse } from 'http';

import useragent from 'useragent';
import { get, isEmpty } from 'lodash';
// import pkoa from 'koa/package.json';
import {
  ClientFields,
  DestinationFields,
  EventSeverity,
  HttpFields,
  HttpRequestFields,
  HttpResponseFields,
  NetworkFields,
  SourceFields,
  URLFields,
  EcsDocumentWithoutBase,
} from 'ecs-logger';

// let base: EcsDocumentWithoutBase | null;

// export function getInformations(): EcsDocumentWithoutBase {
//   if (base == null) {
//     base = {
//       event: {
//         module: 'koa',
//       },
//     };
//   }

//   return base;
// }

export function getFirst<T>(strOrArray: T | string[]) {
  if (Array.isArray(strOrArray)) {
    return strOrArray[0];
  }

  return strOrArray;
}

export type RemoteInfos = {
  address?: string;
  ip?: string;
  nat?: {
    ip?: string;
    port?: number;
  };
  protocol: string;
};

export function getRemoteInfos(req: IncomingMessage): RemoteInfos {
  const { headers, socket } = req;

  const remoteInfos: RemoteInfos = {
    address: socket.remoteAddress,
    ip: socket.remoteAddress,
    protocol: 'http',
  };

  if (!isEmpty(headers['x-forwarded-for'])) {
    remoteInfos.protocol = getFirst(
      get(headers, ['x-forwarded-proto'], 'http')
    );
    remoteInfos.address = getFirst(
      get(headers, ['x-forwarded-for'], socket.remoteAddress)
    );
    remoteInfos.ip = remoteInfos.address;
    remoteInfos.nat = {
      ip: socket.remoteAddress,
      port: socket.remotePort,
    };
  }

  return remoteInfos;
}

// export type EcsDocumentWithoutTimestamp = Optional<EcsDocument, '@timestamp'>;

export default function httpRequestAndResponseToEcs(
  req: IncomingMessage,
  res: ServerResponse
): EcsDocumentWithoutBase {
  const { headers, socket } = req;
  const { method } = req;
  const { statusCode } = res;

  let severity: EventSeverity = EventSeverity.Info;

  if (statusCode >= 500) {
    severity = EventSeverity.Error;
  } else if (statusCode >= 400) {
    severity = EventSeverity.Warning;
  } else if (statusCode >= 300) {
    severity = EventSeverity.Notice;
  }

  const scheme = 'http'; // Detect HTTP scheme here

  const full = `${scheme}://${headers.host}${req.url}`;

  const parsedUrl = url.parse(full, false);
  const userAgent = headers['user-agent'];
  const remoteInfos = getRemoteInfos(req);

  const { bytesWritten, bytesRead } = socket;

  const client: ClientFields = {
    address: remoteInfos.address, // Use x-forwarded-for header here
    bytes: bytesRead,
    ip: remoteInfos.ip, // Use x-forwarded-for header here
    nat: remoteInfos.nat,
    port: socket.remotePort,
  };

  const source: SourceFields = {
    address: remoteInfos.address,
    bytes: bytesRead,
    ip: remoteInfos.ip,
    nat: remoteInfos.nat,
    port: socket.remotePort,
  };

  const destination: DestinationFields = {
    domain: parsedUrl.hostname || undefined,
    bytes: bytesWritten,
    address: socket.localAddress,
    ip: socket.localAddress,
    port: socket.localPort,
  };

  const response: HttpResponseFields = {
    body: {
      bytes: bytesWritten,
    },
    statusCode,
  };

  const request: HttpRequestFields = {
    body: {
      bytes: bytesRead,
    },
    // headers,
    method,
    referrer: headers.referer,
  };

  const http: HttpFields = {
    response,
    request,
    version: req.httpVersion,
  };

  const network: NetworkFields = {
    // ip: socket.localAddress,
    // port: socket.localPort,
    // forwardedIp: socket.remoteAddress,
    protocol: remoteInfos.protocol,
    transport: 'tcp',
    type: 'ipv4',
    direction: 'inbound',
    bytes: bytesRead + bytesWritten,
  };

  const resultUrl: URLFields = {
    domain: parsedUrl.hostname || undefined,
    port: parseInt(parsedUrl.port || '', 10) || undefined,
    path: parsedUrl.path || undefined,
    original: req.url,
    scheme: remoteInfos.protocol,
    full,
    query: parsedUrl.query || undefined,
  };

  const result: EcsDocumentWithoutBase = {
    client,
    source,
    destination,
    http,
    network,
    url: resultUrl,
    event: {
      created: new Date(),
      severity,
    },
  };

  if (userAgent != null) {
    const agent = useragent.parse(userAgent);

    const { device, os } = agent;

    result.userAgent = {
      name: agent.family,
      original: userAgent,
      version: agent.toVersion(),
      device: {
        name: device.family,
      },
      os: {
        family: os.family,
        version: os.toVersion(),
      },
    };
  }

  return result;
}
