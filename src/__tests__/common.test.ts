import { getDuration } from '../common';

describe('common', (): void => {
  describe('#getDuration', (): void => {
    it('should exist', (): void => {
      expect(typeof getDuration).toEqual('function');
    });
  });
});
