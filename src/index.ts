import Koa from 'koa';
import { Logger } from 'ecs-logger';

import koaMiddleware from './koaMiddleware';
import { getCustomLogger } from './customLogger';
import { KoaMiddlewareOpts } from './types';

export * from './common';
export * from './messageParts';
export * from './types';
export { default as formatWithColors } from './messageParts/formatWithColors';
export { default as httpRequestAndResponseToEcs } from './httpRequestAndResponseToEcs';
export {
  default as koaMiddleware,
  genericHttpMiddleware,
} from './koaMiddleware';

export type ErrorWithGraphqlErrors = Error & {
  graphqlErrors: Error[];
};

/* eslint-disable no-undef */
export function isErrorWithGraphqlErrors(
  error: Error | ErrorWithGraphqlErrors
): error is ErrorWithGraphqlErrors {
  return Array.isArray((error as ErrorWithGraphqlErrors).graphqlErrors);
}
/* eslint-enable no-undef */

export function createErrorHandler(logger: Logger) {
  return (error: Error | any): void => {
    if (error.code === 'ECONNRESET') {
      return;
    }
    if (isErrorWithGraphqlErrors(error)) {
      error.graphqlErrors.forEach((graphqlError) => {
        logger.error('Koa graphql error', graphqlError);
      });
    } else if (error.status != null && error.status < 500) {
      logger.warn('Koa warning', undefined, error);
    } else {
      logger.error('Koa error', error);
    }
  };
}

export function useKoaLogger(
  app: Koa,
  opts: Partial<KoaMiddlewareOpts> = {}
): Logger {
  const newLogger = getCustomLogger(opts.logger);
  app.use(koaMiddleware(opts));

  // eslint-disable-next-line no-param-reassign
  app.silent = true;

  app.on('error', createErrorHandler(newLogger));

  return newLogger;
}
