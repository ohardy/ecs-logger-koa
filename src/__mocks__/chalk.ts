const proxy = new Proxy((str: string): string => str || '', {
  get(func: Function, key: string): Function | boolean {
    switch (key) {
      case '__esModule':
        return false;

      case 'magenta':
      case 'cyan':
        return func;
      case 'keyword':
      case 'bgKeyword':
      case 'bgHex':
      case 'hex':
        return () => {
          return proxy;
        };
      default:
    }

    return func;
  },
});

module.exports = proxy;
