import { IncomingMessage, ServerResponse } from 'http';

import { merge } from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import { Context } from 'koa';

import { getDuration } from './common';
import httpRequestAndResponseToEcs from './httpRequestAndResponseToEcs';
import { getCustomLogger } from './customLogger';
import {
  messageFromEcsDocument,
  minimalMessageFromEcsDocument,
} from './messageParts';
import formatWithColors from './messageParts/formatWithColors';
import { KoaMiddlewareOpts } from './types';

export function getOrGetFirst(headerValue: string | string[] | undefined) {
  if (headerValue == null) {
    return null;
  }

  if (Array.isArray(headerValue)) {
    return headerValue[0];
  }

  return headerValue;
}

export type MiddlewareResult = (
  req: IncomingMessage,
  res: ServerResponse,
  next: Function,
  ctx: IncomingMessage | Context
) => Promise<void>;

export function genericHttpMiddleware(
  opts: Partial<KoaMiddlewareOpts> = {}
): MiddlewareResult {
  const { logger, onRequest = (ecsDocument) => ecsDocument } = opts;

  const newLogger = getCustomLogger(logger);

  return async (
    req: IncomingMessage,
    res: ServerResponse,
    next: Function,
    context: IncomingMessage | Context
  ): Promise<void> => {
    const elasticTraceId = getOrGetFirst(
      req.headers['elastic-apm-traceparent']
    );
    const uberTraceId = getOrGetFirst(req.headers['uber-trace-id']);
    const ecsDocument = await Promise.resolve(
      onRequest(
        {
          event: {
            category: ['web'],
            kind: 'event',
            type: 'access',
          },
          trace: {
            id: elasticTraceId || uberTraceId || uuidv4(),
          },
          transaction: {
            id: uuidv4(),
          },
        },
        context,
        req,
        res
      )
    );

    const subLogger = newLogger.createSubLogger({
      ecsDocument,
    });

    // eslint-disable-next-line no-param-reassign
    context.logger = subLogger;
    req.logger = subLogger;
    const timestamp = new Date();
    const startedAt = process.hrtime();
    const startedAtDate = new Date();

    res.on('finish', () => {
      const diff = process.hrtime(startedAt);
      const endedAtEnd = new Date();

      const responseTime = getDuration(diff);

      const newEcsDocument = merge(
        {
          '@timestamp': timestamp,
        },
        httpRequestAndResponseToEcs(req, res),
        {
          event: {
            duration: responseTime,
            start: startedAtDate,
            end: endedAtEnd,
          },
        }
      );

      let coloredMessage;

      if (newLogger.config.renderer.colorEnabled) {
        coloredMessage = messageFromEcsDocument(
          newEcsDocument,
          formatWithColors
        );
      }

      newLogger.logRawDocument({
        ...newEcsDocument,
        message: minimalMessageFromEcsDocument(
          newEcsDocument,
          (_partName: string, value: any) => value
        ),
        coloredMessage,
      });
    });

    await Promise.resolve(next());
  };
}

/**
 * A koa middleware to log request and response.
 */
export default function koaMiddleware(
  opts: Partial<KoaMiddlewareOpts> = {}
): (ctx: Context, next: Function) => Promise<void> {
  const middleware = genericHttpMiddleware(opts);

  return (ctx: Context, next: Function) => {
    return middleware(ctx.req, ctx.res, next, ctx);
  };
  // return , {
  //   onRequest: (ctx: Context, next: Function) => {
  //     return {
  //       next,
  //       req: ctx.req,
  //     };
  //   },
  //   onNewLogger: (logger: Logger) => {
  //     ctx.logger = logger;
  //   },
  // });
}
