import chalk from 'chalk';

export const NS_PER_SEC = 1e9;

export const HTTP_DURATIONS = [
  {
    minDuration: 200,
    colorize: chalk.bgKeyword('red'),
  },
  {
    minDuration: 100,
    colorize: chalk.bgKeyword('yellow').keyword('black'),
  },
  {
    minDuration: 0,
    colorize: chalk.bgKeyword('lightgreen').keyword('black'),
  },
];

export const HTTP_STATUSES = {
  default: {
    colorize: chalk.hex('#008500'),
  },
  2: {
    colorize: chalk.bgKeyword('lightgreen').keyword('black'),
  },
  3: {
    colorize: chalk.bgKeyword('yellow').keyword('black'),
  },
  4: {
    colorize: chalk.bgKeyword('orange').keyword('black'),
  },
  5: {
    colorize: chalk.bgKeyword('red'),
  },
};

export const HTTP_METHODS = {
  default: {
    colorize: chalk.hex('#fff'),
    level: 'info',
  },
  get: {
    colorize: chalk.bgKeyword('lightgreen').keyword('black'),
  },
  post: {
    colorize: chalk.bgKeyword('orange').keyword('black'),
  },
  put: {
    colorize: chalk.bgKeyword('yellow').keyword('black'),
  },
  delete: {
    colorize: chalk.bgKeyword('red'),
  },
};

export const FORMATS = {
  'http.request.method': (value: any): any => {
    return value.toUpperCase();
  },
  'event.duration': (value: any): any => {
    return value;
  },
  'url.path': (value: any): any => {
    return value;
  },
  timestamp: (value: any): any => {
    return value;
  },
  level: (value: any): any => {
    return value.toUpperCase();
  },
  message: (value: any): any => {
    return value;
  },
};

export const MESSAGE_PARTS = [
  'http.request.method',
  'http.response.statusCode',
  // 'event.duration',
  'url.path',
];

export const SIMPLE_MESSAGE_PARTS = ['http.request.method', 'url.path'];

// export const LINE_ELEMENTS = [
//   'timestamp',
//   'level',
//   'module',
//   'message',
//   'error',
// ];
