import { NS_PER_SEC } from './constants';

/**
 * Return duration in nanoseconds from a diff
 */
export function getDuration(diff: [number, number]): number {
  return diff[0] * NS_PER_SEC + diff[1];
}
