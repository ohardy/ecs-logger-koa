import formatWithColors from '../formatWithColors';

describe('messageRenderers - formatWithColors', (): void => {
  it('should exist', (): void => {
    expect(typeof formatWithColors).toEqual('function');
  });
});
