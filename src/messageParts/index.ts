import { get } from 'lodash';
// import format from 'date-fns/format';

import { EcsDocument } from 'ecs-logger';

// import formatWithoutColors from './formatWithoutColors';

import {
  // LINE_ELEMENTS,
  MESSAGE_PARTS,
  SIMPLE_MESSAGE_PARTS,
} from '../constants';

// export function createFormatPart(colorize: boolean): Function {
//   if (colorize) {
//     return formatWithColors;
//   }

//   return formatWithoutColors;
// }

// export function formatLine(
//   obj: { [key: string]: any },
//   doFormatPart: Function
// ): string {
//   const parts = [];
//   for (const part of LINE_ELEMENTS) {
//     const value = get(obj, part);
//     if (value != null) {
//       parts.push(doFormatPart(part, value));
//     }
//   }

//   return parts.join(' ');
// }

export function messageFromEcsDocument(
  ecsDocument: Omit<EcsDocument, 'message'>,
  doFormatPart: Function
) {
  const parts = [];

  for (const part of MESSAGE_PARTS) {
    const value = get(ecsDocument, part);
    if (value != null) {
      parts.push(doFormatPart(part, value));
    }
  }

  return parts.join(' ');
}

export function minimalMessageFromEcsDocument(
  ecsDocument: Omit<EcsDocument, 'message'>,
  doFormatPart: Function
) {
  const parts = [];

  for (const part of SIMPLE_MESSAGE_PARTS) {
    const value = get(ecsDocument, part);
    if (value != null) {
      parts.push(doFormatPart(part, value));
    }
  }

  return parts.join(' ');
}

// export function getMessage(
//   logEvent: LogEvent | FinalLogEvent,
//   minimal: boolean = true,
//   doFormatPart: Function
// ) {
//   if (logEvent.message != null) {
//     return logEvent.message;
//   }

//   if (minimal) {
//     return logEventToMinimalMessage(logEvent, doFormatPart);
//   }

//   return logEventToMessage(logEvent, doFormatPart);
// }

// export function getLineParts<T extends boolean>(
//   logEvent: FinalLogEvent,
//   minimal: T,
//   colorize: boolean
// ): { [key: string]: any } {
//   const formatPart = createFormatPart(colorize);

//   const message = getMessage(logEvent, minimal, formatPart);

//   if (minimal) {
//     return { message };
//   }

//   return {
//     error: logEvent?.error?.__original,
//     module: logEvent?.labels?.module,
//     timestamp: format(logEvent['@timestamp'], 'HH:mm:ss'),
//     level: logEvent.log.level,
//     message,
//   };
// }
