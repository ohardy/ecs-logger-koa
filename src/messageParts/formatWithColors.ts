import { get, toString } from 'lodash';

import {
  // APP_LABELS_MODULE_COLOR,
  HTTP_METHODS,
  HTTP_STATUSES,
  // LEVEL_COLORS,
} from '../constants';
// import { colorizeDuration } from '../common';

const FORMATS: { [key: string]: Function } = {
  'http.response.statusCode': (value: any): string => {
    const func = get(HTTP_STATUSES, [toString(value)[0]], HTTP_STATUSES.default)
      .colorize;

    return func(` ${value} `);
  },
  'http.request.method': (value: any): string => {
    const func = get(HTTP_METHODS, [toString(value)], HTTP_METHODS.default)
      .colorize;

    return func(` ${value.toUpperCase()} `);
  },
  // 'event.duration': (value: any): string => {
  //   return colorizeDuration(value);
  // },
  // module: (value: string | undefined): any => {
  //   if (value != null) {
  //     return APP_LABELS_MODULE_COLOR(` ${value.toUpperCase()} `);
  //   }
  //   return '';
  // },
  'url.path': (value: any): any => {
    return value;
  },
  // timestamp: (value: any): any => {
  //   return value;
  // },
  // error: (error: any): any => {
  //   if (error.stack != null) {
  //     const stack = error.stack
  //       .split('\n')
  //       .map((str: string) => {
  //         return trim(str);
  //       })
  //       .join('\n');
  //     return `\n${stack}`;
  //   }
  //   return '';
  // },
  // level: (value: any): any => {
  //   let level: string = value.toLowerCase();
  //   const colorizeFunc: Function | undefined = LOG_LEVEL_COLORS[level];
  //   const levelLength = level.length;
  //   level = ` ${value.toUpperCase()}${pad(' ', Math.max(0, 6 - levelLength))}`;

  //   if (colorizeFunc != null) {
  //     level = colorizeFunc(level);
  //   }

  //   return level;
  // },
  // message: (value: any): any => {
  //   return value;
  // },
};

export default function formatWithColors(partName: string, value: any): string {
  const func = FORMATS[partName];

  if (func != null) {
    return func(value);
  }

  return value;
}
