import { IncomingMessage, ServerResponse } from 'http';

import { Context } from 'koa';
import { EcsDocumentWithoutBase, Logger } from 'ecs-logger';

declare module 'koa' {
  interface ExtendableContext {
    logger: Logger;
  }
}

declare module 'http' {
  interface IncomingMessage {
    logger: Logger;
  }
}

export type KoaMiddlewareOnRequest = (
  ecsDocument: EcsDocumentWithoutBase,
  context: IncomingMessage | Context,
  req: IncomingMessage,
  res: ServerResponse
) => Promise<EcsDocumentWithoutBase> | EcsDocumentWithoutBase;

export type KoaMiddlewareOpts = {
  logger: Logger;
  onRequest?: KoaMiddlewareOnRequest;
};
